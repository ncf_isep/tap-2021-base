name := "tap-2021-base"

version := "0.1"

scalaVersion := "3.0.0-RC2"

scalacOptions ++= Seq("-source:future", "-indent", "-rewrite")

// XMl
libraryDependencies += "org.scala-lang.modules" %% "scala-xml" % "2.0.0-RC1"

// Scalatest
libraryDependencies += "org.scalactic" %% "scalactic" % "3.2.7"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.7" % "test"

// Scalacheck
libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.15.3" % "test"
