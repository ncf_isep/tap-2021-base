package assessment

import scala.xml.Elem
import domain.Result
import domain.schedule.{Schedule, ScheduleMS01}

object AssessmentMS01 extends Schedule:
  def create(xml: Elem): Result[Elem] = ScheduleMS01.create(xml)

